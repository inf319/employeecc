package inf319ng.employeecc;

    public class Engenheiro extends Empregado {
    
    private double comissaoProjeto;

    public double getComissaoProjeto() {
	return comissaoProjeto;
    }

    public void setComissaoProjeto(double comissaoProjeto) {
	this.comissaoProjeto = comissaoProjeto;
    }

	@Override
	public double salario() {
		return getSalario() + getComissaoProjeto();
	}

}
