package inf319ng.employeecc;

/**
 * Escreva a sua avaliacao do projeto atual . Seja conciso e preciso.
 * - Este projeto utiliza a noção de herança. As classes Gerente, Vendedor e Engenheiro 
 * herdam da classe Empregado;
 * - Deve-se notar porém, que o método para calcular o salário está na classe Empregado.
 * Isso cria um problema pois para calcular o salario de uma classe filha, precisamos primeiro 
 * testar qual a classe estamos utilizando e aí utilizar uma estratégia específica para calcular;
 * - Outro problema que isto pode acarretar é que caso precisamos adicionar uma nova classe que
 * herda de Empregado, precisariamos criar outra condição no código. 
 * Adicione novos comentarios para explicar a sua melhoria.
 * - Para melhorar o projeto, a classe Empregado tornou-se abstrata. Agora, não podemos mais 
 * instanciar um Empregado;
 * - Tornamos o método salário() abstrato também, assim a responsabilidade de calcular o salário 
 * foi para as classes filhas. Agora cada classe filha deve implementar seu algoritmo para o cálculo; 
 * - Por fim, foi adicionado o método getSalario() para pegar o valor do atributo salário.
 * @author INF319
 */
public abstract class Empregado {

    private double salario;

    public void setSalario(double salario) {
        this.salario = salario;
    }
    
    public double getSalario() {
        return salario;
    }

    public abstract double salario();

}
